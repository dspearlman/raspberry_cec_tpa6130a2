CXX=g++
CXXFLAGS=-Wall -g -std=c++11
OBJDIR=./obj
SRCDIR=./src

SRCS = $(SRCDIR)/cec-volume.cpp $(SRCDIR)/cec-client.cpp $(SRCDIR)/i2c-amp.cpp

OBJ=$(patsubst $(SRCDIR)/%.cpp,$(OBJDIR)/%.o,$(SRCS))

LDLIBS=-ldl -lp8-platform -pthread -lcec

INCLUDES=-I./include -I/usr/local/include/libcec/ -I/usr/local/include/p8-platform/ -I./build_deps/libcec/src/libcec/ -I./build_deps/libcec/src/libcec/devices/

MAIN=cec-volume

.PHONY: depend

all: $(MAIN)
	@echo Compilation complete

$(MAIN): $(OBJ) 
	$(CXX) $(CXXFLAGS) $(INCLUDES) $(LDLIBS) -o $(MAIN) $(OBJ)


$(OBJDIR)/%.o: $(SRCDIR)/%.cpp 
	@echo "Compiling: " $@
	$(CXX) $(CXXFLAGS) $(INCLUDES) $(LDLIBS) -c -o $@ $< 


clean:
	$(RM) $(OBJDIR)/*.o *~ $(MAIN)

depend: $(SRCS)
	makedepend $(INCLUDES) $^

# DO NOT DELETE THIS LINE -- make depend needs it
