# Raspberry_CEC_TPA6130A2

Listens to CEC commands and manages a TPA6130A2 headphone amplifier's volume controls accordingly over i2c.

This chip is found in the gaia vision SPDIF to RCA/3.5mm adapter with remote control, GV-A006-v2.0-1, also known as GV-CA1103
https://www.gaia-vision.com/product/gv-ca1103-dac-amp-by-remote-control/

[Amazon link](https://www.amazon.com/gp/product/B07LF82S95/ref=ppx_yo_dt_b_asin_title_o07_s00?ie=UTF8&psc=1)

**Reason for this project: **
I was unhappy with the fact that the remote control only adjusted volume to 10 different levels, despite the TPA6130A2 supporting 64 volume levels. It was either too quiet or too loud, never just right. I tried two other adapters as well that used HDMI ARC and CEC, the first one had horrible audio quality, and the second one had strange volume steps that confused my Visio smart TV's interraction with Google Home, so that saying "Hey Google, set volume to 20%" would seem to pick at random either 24, 30, 15, or 5%. Both ARC adapters were also extremely slow to respond to remote input, sometimes not responding at all. 

This project should allow normal TV remote as well as Google Home control (with a smart TV) over the SPDIF adapter.

[TI TPA6130A2 datasheet](https://www.ti.com/lit/ds/symlink/tpa6130a2.pdf)

**Hardware Modifications**
A few modifications need to be made to the board so the volume control can be done by the rpi, and the built in microcontroller can continue to control the SPDIF converter chip. The SPDIF converter chip and the TPA6130A2 share an i2c bus, and unfortunately the wiring is Microcontroller -> TPA6130A2 -> SPDIF converter. On the TPA6130A2, SDA is pin 7, and SCL is pin 8. See page 6 of the datasheet, and if you're not sure, use a multimeter to probe continuity to R26 and R27 (SDA and SCL respectively). If you're _very_ good with soldering, you can cut the traces close to the TPA6130A2 (before the vias) and add wires directly to the chip, and no further modification is needed. I found it easier to cut the traces on the back of the board and solder some wire-wrap wire directly to the traces (super gluing afterwards for strain relief), but this disconnects the micro from the SPDIF converter. I added softwires to the top of the board re-connecting them. There are pictures located in the top level of this repository for reference


**Setting up rpi**
First, do the usual setup. I used Raspbian, but I imagine other distros are similar in setup
Enable i2c from the raspberry pi configuration menu (or raspi-config from terminal)
Run build\_deps.sh to build the supporting libraries
Note: build\_deps.sh will add two folders to your home directory, platform and libcec. They can be removed after everything is setup
I recommend you glance at my build\_deps.sh script before running it, it's pretty quick and dirty

