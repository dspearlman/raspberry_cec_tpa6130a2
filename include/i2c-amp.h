#ifndef __I2C_AMP_H
#define __I2C_AMP_H

/* This is for a TPA6130A2 Amplifier from TI */
class I2CAmp {
private:

  int m_handle;
  bool m_enabled;
  uint8_t m_volumeMuteReg;
  void write_reg(uint8_t reg, uint8_t data);

public:

  I2CAmp();
  // Set 6-bit volume, overflow defaults to maximum
  void setVolume(uint8_t vol);

  // Turns on/off mute
  void setMute(bool val); 

  // ...standby and enable
  void standby();
  void enable();
};

  
#endif

