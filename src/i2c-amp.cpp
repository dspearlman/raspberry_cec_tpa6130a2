#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <csignal>
#include <unistd.h>
#include <iostream>
#include "i2c-amp.h"


#define I2C_DEVICE_ADDRESS (0x60)
#define CONTROL_REGISTER (0x01)
#define CTRL_STANDBY (0x01)
#define CTRL_ENABLE (0xC0)
#define VOLUME_MUTE_REGISTER (0x02)
#define VOLUME_MASK (0x3F)

void I2CAmp::write_reg(uint8_t reg, uint8_t data) {
  char buf[2];
  buf[0] = reg;
  buf[1] = data;
  if (write(m_handle, buf, 2) != 2) {
    std::cerr << "Write to /dev/i2c-1 reg " << reg << " data " << data << " failed\n";
    std::raise(SIGTERM);
  }
}

I2CAmp::I2CAmp(){
  m_handle = open("/dev/i2c-1", O_RDWR);
  if (m_handle < 0) {
    std::cerr << "Couldn't open /dev/i2c-1 for the audio amp, aborting\n";
    std::raise(SIGTERM);
  }

  if (ioctl(m_handle, I2C_SLAVE, I2C_DEVICE_ADDRESS) < 0){
    std::cerr << "Failed to set i2c slave address\n";
    std::raise(SIGTERM);
  }
  m_volumeMuteReg = 0;
  I2CAmp::standby();
}


void I2CAmp::setVolume(uint8_t vol) {
  if (vol > VOLUME_MASK){
    vol = VOLUME_MASK;
  }
  m_volumeMuteReg &=  ~VOLUME_MASK;
  m_volumeMuteReg |= vol;
  if (!m_enabled) {
    write_reg(CONTROL_REGISTER, 0); // Brings amp chip out of standby, but doesn't enable amp
  }
  write_reg(VOLUME_MUTE_REGISTER, m_volumeMuteReg);
  I2CAmp::enable();
}

void I2CAmp::setMute(bool val) {
  if (val) {
    m_volumeMuteReg |= ~VOLUME_MASK;
    write_reg(VOLUME_MUTE_REGISTER, m_volumeMuteReg);
  }
  else {
    m_volumeMuteReg &= VOLUME_MASK;
    write_reg(VOLUME_MUTE_REGISTER, m_volumeMuteReg);
  }
}

void I2CAmp::standby() {
  write_reg(CONTROL_REGISTER, CTRL_STANDBY);
  m_enabled = false;
}

void I2CAmp::enable(){
  write_reg(CONTROL_REGISTER, CTRL_ENABLE);
  m_enabled = true;
}


