#include "env.h"
#include "cec.h"

#include "i2c-amp.h"

//Not public interfaces, but they don't do what I need...
#include "CECAudioSystem.h"
#include "CECProcessor.h"
#include "LibCEC.h"

#include <csignal>
#include <iostream>
using namespace CEC;

extern ICECAdapter*          g_parser;

static bool                  initRan = false;
static CLibCEC*              cecAdapter = nullptr;
static CCECAudioSystem*      audioSystem = nullptr;
static uint8_t               volume = 15;
static I2CAmp*               amplifier;

#define MUTE_STATUS_VALUE (0x80)

void setAmpVolume(uint8_t libcecVol) {
  amplifier->setVolume(libcecVol & ~MUTE_STATUS_VALUE);
  amplifier->setMute(libcecVol & MUTE_STATUS_VALUE);
}

/* Run once after libcec has done its thing
 * libcec doesn't seem to provide an interface to this, so I've decided to stop spending time
 * finding if it's possible through their API, and just reach into their internals instead.
 */
void VolumeInit() {
   if (initRan){
    return;
  }
  cecAdapter = dynamic_cast<CLibCEC*>(g_parser);
  if (!cecAdapter) {
    std::cerr << "FATAL ERROR: Couldn't cast g_parser from ICECAdapter* to CLibCEC*!!!!!!!!!!!!!\n";
    std::raise(SIGTERM);
  }

  audioSystem = cecAdapter->m_cec->GetAudioSystem();
  if (!audioSystem) {
    std::cerr << "FATAL ERROR: Couldn't find the audio device, was it not initialized? cec-client.cpp should be adding CEC_DEVICE_TYPE_AUDIO_SYSTEM to g_config.deviceTypes\n";
    std::raise(SIGTERM);
  }

  if (!audioSystem->SetAudioStatus(volume)){
    std::cerr << "FATAL ERROR: Couldn't set volume to zero\n";
    std::raise(SIGTERM);
  }
  amplifier = new I2CAmp();

  audioSystem->TransmitCECVersion(CECDEVICE_TV, false);
  audioSystem->SetPowerStatus(CEC_POWER_STATUS_ON);

  // CEC spec says to address the TV directly when requesting active source, libCEC broadcasts
  cec_command command;
  cec_command::Format(command, CECDEVICE_AUDIOSYSTEM, CECDEVICE_TV, CEC_OPCODE_REQUEST_ACTIVE_SOURCE);
  g_parser->Transmit(command);

  audioSystem->SetSystemAudioModeStatus(CEC_SYSTEM_AUDIO_STATUS_ON);
  audioSystem->TransmitAudioStatus(CECDEVICE_TV, false);
  audioSystem->TransmitSetSystemAudioMode(CECDEVICE_TV, false);

  amplifier->enable();
  setAmpVolume(volume);

  initRan = true;
}


void CecKeyPress(void *UNUSED(cbParam), const cec_keypress* UNUSED(key))
{  
}

void CecSourceActivated(void* UNUSED(cbParam), const cec_logical_address logicalAddress, const uint8_t bActivated){
    g_parser->SetInactiveView();
}

void CecCommand(void *UNUSED(cbParam), const cec_command* command)
{
  switch(command->opcode) {
    case CEC_OPCODE_USER_CONTROL_PRESSED : {
      switch(command->parameters.data[0]) {
        case CEC_OPCODE_REPORT_POWER_STATUS :
          audioSystem->TransmitPowerState(CECDEVICE_TV, true);
          break;

        case CEC_USER_CONTROL_CODE_VOLUME_UP :
          if ((volume & ~MUTE_STATUS_VALUE) < 100) {
            volume ++;
          }
          break;

        case CEC_USER_CONTROL_CODE_VOLUME_DOWN :
          if (volume > 0) {
            volume --;
          }
          break;

        case CEC_USER_CONTROL_CODE_MUTE :
          volume ^= MUTE_STATUS_VALUE;
          // Mute as soon as possible
          setAmpVolume(volume); 
          break;

        default: {
          break;
        }
      }
      break;
    }

    case CEC_OPCODE_STANDBY : {
      amplifier->standby();
      volume = 15;
      audioSystem->SetAudioStatus(volume);
      break;
    }

    case CEC_OPCODE_REQUEST_ARC_START : {
      std::cout << "WATERMELON\n";
      amplifier->enable();
      setAmpVolume(volume);
      break;
    }
    
    case CEC_OPCODE_VENDOR_REMOTE_BUTTON_UP : {
    //case CEC_OPCODE_USER_CONTROL_RELEASE : {
      switch(command->parameters.data[0]) {
        case CEC_USER_CONTROL_CODE_VOLUME_UP :
        case CEC_USER_CONTROL_CODE_VOLUME_DOWN :
        case CEC_USER_CONTROL_CODE_MUTE :
          if ((volume & ~MUTE_STATUS_VALUE) > 100) { //Should never happen
            volume = 15;
          }
          setAmpVolume(volume);
          audioSystem->SetAudioStatus(volume);
          audioSystem->TransmitAudioStatus(CECDEVICE_TV, false);
          break;

        default: {
          break;
        }
      }
      break;
    }

    default : {
      break;
    }
  }
}

