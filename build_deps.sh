#!/bin/bash
mkdir -pv obj
mkdir -pv build_deps
pushd build_deps
sudo apt-get update
sudo apt-get install cmake liblockdev1-dev libudev-dev libxrandr-dev python-dev swig
git clone https://github.com/Pulse-Eight/platform.git
mkdir platform/build
pushd platform/build
cmake ..
make
sudo make install
popd
git clone https://github.com/Pulse-Eight/libcec.git
mkdir libcec/build
pushd libcec/build
cmake -DRPI_INCLUDE_DIR=/opt/vc/include -DRPI_LIB_DIR=/opt/vc/lib ..
make -j2
sudo make install
sudo ldconfig
popd
cp ./libcec/src/libcec/env.h ./include/env.h
popd
